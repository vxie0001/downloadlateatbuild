# /bin/bash


if [ $# != 1  ]
  then
  echo "please input product name. Current product name can be \"kiwi-uxdevelop-methane-release\" and \" kiwi-uxdevelop-release\". only one parameter is allowed"
  exit
fi  

TOMCAT_WEBAPP_DIR="/opt/imail1/tomcat-8080/webtop/webapps/"
RELEASE_APP_NAME="kiwi-uxdevelop-methane-release-cpms"
DOWNLOAD_REPO="http://172.26.64.28:8081/artifactory/libs-release-local/com/owmessaging/webtop/studios/"
DOWNLOAD_PRODUCT=$1

#kill tomcat 

#ps -ef | grep "java" |grep "tomcat-8080"|awk '{print $2}'|xargs kill -9

cd /opt/imail1/webtop_tool_cpms 

# stop web app 
lib/restart_tomcat_or_webapp.sh    $RELEASE_APP_NAME  appStop

cd  $TOMCAT_WEBAPP_DIR


# get the lastest build version
BUILDID=`curl  $DOWNLOAD_REPO$DOWNLOAD_PRODUCT/maven-metadata.xml  |grep "<latest>" | awk  -F '<'  '{print $2}' | awk  -F '>'  '{print $2}' `

echo  $BUILDID

DOWNLOAD_URL=$DOWNLOAD_REPO$DOWNLOAD_PRODUCT"/"$BUILDID"/"$DOWNLOAD_PRODUCT"-"$BUILDID".war"

WAR_NAME=$DOWNLOAD_PRODUCT"-"$BUILDID".war"

#If the same war file exists, delete it
if [ -f $WAR_NAME ]
  then rm $WAR_NAME
fi

#download the build
wget $DOWNLOAD_URL  


# Remove the existing release webapp and war file
if [ -d $RELEASE_APP_NAME ]
    then   rm -fr $RELEASE_APP_NAME/*
fi
if [ -f $RELEASE_APP_NAME".war" ]
  then   rm $RELEASE_APP_NAME".war"
fi


WAR_NAME=$DOWNLOAD_PRODUCT"-"$BUILDID".war"

#rename the war file that has just been download 
mv $WAR_NAME $RELEASE_APP_NAME".war"

# unzip the war file  
unzip -o $RELEASE_APP_NAME".war" -d $RELEASE_APP_NAME

#start the webapp 
#/opt/imail1/webtop_tool_cpms/lib/restart_tomcat_or_webapp.sh    $RELEASE_APP_NAME appStart


#cd -
